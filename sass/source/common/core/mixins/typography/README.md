# @font

Expands the font shorthand in order to avoid wrong inheritance or involuntary property reset.
[Check more info here.](http://www.impressivewebs.com/a-primer-on-the-css-font-shorthand-property/)

* **IMPORTANT 1:** commom parameters __normal__ and __inherit__ will be written in the shorthand order: font-style, font-variant, font-weight
* **IMPORTANT 2:** font-size / line-height not supported! Add your line-height manually. Lets wait for SASS 3.3 in order to be able to manipulate strings!

## Parameters

1. $args {List} : font-style, font-variant, font-weight, font-size, font-family

## Dependencies

From [SassyLists](https://github.com/Team-Sass/SassyLists/blob/master/stylesheets/SassyLists/)

* replace()
* remove()
* contain()
* purge()

## Examples

	.example1{	@include font(italic);}

	.example2{	@include font("Helvetica Neue"); }

	.example3{	@include font("Helvetica Neue" Arial sans); }

	.example4{	@include font(2rem); }
	.example5{	@include font(12px); }
	.example6{	@include font(50%); }
	.example7{	@include font(1.5em); }

	.example8{	@include font(bold); }

	.example9{	@include font(italic, "Helvetica Neue" Arial sans, 12rem, bold); }

	.example10{	@include font(bolder, 2em); }

	.example11{	@include font("Helvetica Neue" Arial sans, italic, bold, 3rem); }

	.example12{	@include font(300, 20px, "Helvetica Neue" Arial sans, italic); }

	.example13{	@include font(inherit, inherit, normal); }
	.example14{	@include font("Helvetica Neue" Arial sans, normal); }
	.example15{	@include font("Helvetica Neue" Arial sans, normal, inherit); }
	.example16{	@include font("Helvetica Neue" Arial sans, normal, inherit, normal); }
	.example17{	@include font("Helvetica Neue" Arial sans, normal, inherit, normal, 12px); }
	.example18{	@include font(12px, "Helvetica Neue" Arial sans, normal, inherit, normal); }
	.example19{	@include font(normal, 12px, inherit, "Helvetica Neue" Arial sans, normal); }
	.example20{	@include font(bold, 12px, "Helvetica Neue" Arial sans); }